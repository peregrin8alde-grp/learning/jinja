# MyJ2Cli

テンプレートの動作確認用の簡易クライアント。
j2cli というツールはいくつか存在するが、Jinja 公式は見当たらなかったため自作。

## Jinja

Jinjaは以下の方法で入手、利用している。

```
mkdir lib && \
pip install Jinja2 -t ./lib
```

```
export PYTHONPATH=lib
```

## 実行

```
./j2cli.sh sample/j2 sample.txt.j2 sample/config/sample.json
```
