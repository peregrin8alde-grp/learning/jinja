#!/bin/bash

SCRIPT_DIR=$(cd "$(dirname "${BASH_SOURCE:-$0}")" && pwd)

export PYTHONPATH=${SCRIPT_DIR}/lib

TEMPLATE_DIR=$1
TEMPLATE_FILE=$2
CONFIG_FILE=$3

python module/myj2cli.py $1 $2 $3


exit 0
